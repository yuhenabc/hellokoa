const Koa = require('koa');
const app = new Koa();

const one = (ctx, next) => {
  console.log('====== start ======');
  next();
  console.log('======  end  ======');
};

const two = (ctx, next) => {
  console.log('>>>>>> start <<<<<<');
  next();
  console.log('>>>>>>  end  <<<<<<');
};

const final = (ctx) => {
  console.log('I am final!');
  ctx.body = 'Hello World\n';
};

app.use(one);
app.use(two);
app.use(final);

app.listen(3000, () => {
  console.log('server listening on port 3000.\n');
});
